# WARNING

WIP, only one utility at the moment

# Description

dietnusse's collection of useful utilities written in POSIX shell, intended for Linux

# Utilities

qchroot

# Dependencies

POSIX shell \
util-linux

# Instructions

To install into /usr/local (note that DESTDIR and PREFIX are accepted):
```
# make install
```
That's it!
