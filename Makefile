PREFIX = /usr/local

all:
	@echo run \'make install\' to install useful-utils

install:
	install -Dm755 sh/qchroot $(DESTDIR)$(PREFIX)/bin/qchroot
	
uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/qchroot
